#ifndef OVERWATCHVISION_SAMPLEGENERATION_H
#define OVERWATCHVISION_SAMPLEGENERATION_H

#include <iostream>
#include <opencv/cv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <string>

typedef std::vector<cv::Mat>(distortFunc)(const cv::Mat& original);

distortFunc addNoise;
distortFunc addBlur;
distortFunc addScale;
distortFunc addRotation;
distortFunc addPerspective;

/**
 * Generate distorted samples based on input to increase machine learning
 * training robustness.
 *
 * @param filename Input image filename
 */
void
generateDistortions(std::string filename)
{
  std::vector<cv::Mat> results = { cv::imread(filename) };
  std::vector<distortFunc*> functions = {
    addPerspective, addScale, addRotation, addBlur, addNoise
  };
  for (const auto& fn : functions) {
    std::vector<cv::Mat> tmp;
    for (const auto& r : results) {
      const auto distorted = fn(r);
      tmp.insert(tmp.end(), distorted.begin(), distorted.end());
    }
    std::swap(tmp, results);
  }

  const auto extStart = filename.find_last_of(".");
  const auto baseName = filename.substr(0, extStart);
  const auto ext = filename.substr(extStart);
  for (std::size_t i = 0; i < results.size(); i++) {
    std::stringstream outName;
    outName << baseName << ".distorted-" << i << ext;
    const auto image = results[i];
    if (!cv::imwrite(outName.str(), image)) {
      std::cout << "ERROR: Could not save sample to: " << outName.str() << "\n";
      exit(1);
    }
  }
}

std::vector<cv::Mat>
addNoise(const cv::Mat& original)
{
  std::vector<cv::Mat> results;
  for (const auto noiseLevel : { 0, 3, 6 }) {
    cv::Mat noise(original.size(), original.type());
    const auto stdDev = 255 * noiseLevel / 100;
    cv::randn(noise, cv::Scalar(0, 0, 0), cv::Scalar(stdDev, stdDev, stdDev));
    results.emplace_back(original + noise);
  }
  return results;
}

std::vector<cv::Mat>
addBlur(const cv::Mat& original)
{
  std::vector<cv::Mat> results;
  results.emplace_back(original.clone());
  for (const auto size : { 1, 3 }) {
    cv::Mat blurred;
    cv::blur(original, blurred, cv::Size(size, size));
    results.emplace_back(blurred);
  }
  return results;
}

std::vector<cv::Mat>
addScale(const cv::Mat& original)
{
  std::vector<cv::Mat> results;
  for (const auto scale : { 95, 100, 105 }) {
    cv::Mat output(original.size(), original.type());
    double scaleDecimal = scale / 100.0;
    cv::resize(original, output, cv::Size(0, 0), scaleDecimal, scaleDecimal);
    results.push_back(output);
  }
  return results;
}

std::vector<cv::Mat>
addRotation(const cv::Mat& original)
{
  std::vector<cv::Mat> results;
  for (const auto angle : { -2, 0, 2 }) {
    const auto center = cv::Point(original.cols / 2, original.rows / 2);
    const auto M = cv::getRotationMatrix2D(center, angle, 1.0);
    cv::Mat rotated;
    // This is fills interior edges with black and crops exterior corners, but
    // good enough.
    cv::warpAffine(original, rotated, M, original.size(), cv::INTER_CUBIC);
    results.push_back(rotated);
  }
  return results;
}

std::vector<cv::Mat>
addPerspective(const cv::Mat& original)
{
  std::vector<cv::Mat> results;
  // Top-left, top-right, bottom-right, bottom-left.
  const std::vector<cv::Point> sourcePts = {
    cv::Point(0, 0),
    cv::Point(original.cols, 0),
    cv::Point(original.cols, original.rows),
    cv::Point(0, original.rows),
  };

  const double deltaValue = 0.03;
  std::vector<std::pair<double, double>> deltas = { { 0, 0 },
                                                    { deltaValue, 0 },
                                                    { -1 * deltaValue, 0 },
                                                    { 0, deltaValue },
                                                    { 0, -1 * deltaValue } };

  for (const auto d : deltas) {
    const auto dX = static_cast<int>(original.cols * d.first);
    const auto dY = static_cast<int>(original.rows * d.second);
    const std::vector<cv::Point> destPts = {
      sourcePts[0],
      sourcePts[1],
      cv::Point(sourcePts[2].x - dX, sourcePts[2].y - dY),
      cv::Point(sourcePts[3].x + dX, sourcePts[3].y + dY)
    };
    const auto H = cv::findHomography(sourcePts, destPts);
    cv::Mat transformed;
    cv::warpPerspective(original, transformed, H, original.size());
    results.push_back(transformed);
  }

  return results;
}

#endif // OVERWATCHVISION_SAMPLEGENERATION_H
