#include "../../main/jni/misc.h"
#include "../../main/jni/pipeline.h"
#include "../../main/jni/pipelineBuilder.h"
#include "../../main/jni/results.h"
#include "../../main/jni/stages/binarizeStage.h"
#include "../../main/jni/stages/convertInputStage.h"
#include "../../main/jni/stages/detectMedalsStage.h"
#include "../../main/jni/stages/detectOutcomeStage.h"
#include "../../main/jni/stages/detectStatsStage.h"
#include "../../main/jni/stages/recognizeTextMlStage.h"
#include "opencv2/opencv.hpp"
#include "sampleGeneration.h"
#include "training.h"
#include <functional>
#include <iostream>
#include <numeric>
#include <string>

void
displayResults(const std::string& filename,
               const RecognitionResults& results,
               bool showImage);

int
main(int argc, char** argv)
{
  if (argc < 4) {
    std::cout << "Missing args. Must have 1) training file, 2)command and 3) "
                 "filename(s)\n";
    return -1;
  }
  std::string trainingFile = argv[1];
  std::string command = argv[2];

  if (command == "run") {
    const auto showImage = argc < 50;
    auto pipeline = buildPipeline(
      0.85, InputParams{ false, cv::COLOR_BGRA2BGR }, trainingFile);

    for (int argIdx = 3; argIdx < argc; argIdx++) {
      std::string filename = argv[argIdx];
      std::cout << "processing: " << filename << "\n";

      auto input = cv::imread(filename, cv::IMREAD_COLOR);
      auto results = pipeline(input);

      displayResults(filename, results, showImage);
      // Show individual digits if only doing a single image
      // if (argc == 4) {
      //   for (const auto& statDigits : results.detected.stats) {
      //     for (const auto& digit : statDigits) {
      //       cv::namedWindow("digit", cv::WINDOW_NORMAL);
      //       cv::imshow("digit", digit);
      //       while (cv::waitKey(0) != 'q')
      //         ;
      //     }
      //   }
      // }
    }

    if (showImage) {
      std::cout << "Press 'q'/ctrl-c to exit\n";
      while (cv::waitKey(30) != 'q')
        ;
    } else {
      std::cout << "WARNING: Too many images to display\n";
    }
  } else if (command == "train") {
    auto pipeline = buildPipeline(
      0.85, InputParams{ false, cv::COLOR_BGRA2BGR }, std::string());
    training_data_t trainingData;
    for (int argIdx = 3; argIdx < argc; argIdx++) {
      std::string filename = argv[argIdx];
      std::cout << "Extracting training data: " << filename << "\n";
      auto input = cv::imread(filename, cv::IMREAD_COLOR);
      auto results = pipeline(input);
      labelData(results.detected, filename, trainingData);
    }
    std::cout << "Training model... samples: " << trainingData.size() << "\n";
    const auto statModel = RecognizeTextMlStage::trainModel(trainingData);
    statModel->save(trainingFile);
    std::cout << "Done training\n";
  } else if (command == "genSamples") {
    for (int argIdx = 3; argIdx < argc; argIdx++) {
      std::string filename = argv[argIdx];
      std::cout << "Generating samples from: " << filename << "\n";
      generateDistortions(filename);
    }
  } else {
    std::cout << "Unknown command: " << command << "\n";
    return -1;
  }

  return 0;
}

void
displayResults(const std::string& filename,
               const RecognitionResults& results,
               bool showImage)
{
  const auto& detected = results.detected;
  std::cout << "\t# medals:       " << detected.medals.size() << "\n";
  std::cout << "\t# stat digits:  " << detected.stats.size() << " [";
  for (const auto& s : detected.stats) {
    std::cout << s.size() << ", ";
  }
  std::cout << "]\n";
  std::cout << "\tstat values:    ";
  for (const auto& t : results.stats) {
    std::cout << t << ", ";
  }
  std::cout << "\n";
  std::cout << "\toutcome:        " << (detected.outcome.empty() ? "no" : "yes")
            << "\n";

  auto expectedStats = loadMetadata(filename);
  if (!expectedStats.empty() && !results.stats.empty()) {
    int incorrect = 0;
    for (std::size_t s = 0; s < expectedStats.size(); s++) {
      const auto& currentExpected = expectedStats[s];
      if (s > results.stats.size()) {
        incorrect += currentExpected.size();
        continue;
      }
      const auto& currentResult = results.stats[s];

      auto previousIncorrect = incorrect;
      for (std::size_t d = 0; d < currentExpected.size(); d++) {
        if (d > currentResult.size()) {
          incorrect += currentExpected.size() - currentResult.size();
        } else if (currentExpected[d] != currentResult[d]) {
          incorrect++;
        }
      }
      if (previousIncorrect != incorrect) {
        std::cout << "\t\tIncorrect stat. Expected: '" << currentExpected
                  << "', actual: '" << currentResult << "'\n";
      }
    }
    if (incorrect != 0) {
      std::cout << "\trecognition errors: " << incorrect << "\n";
    } else {
      std::cout << "\trecognition errors: none\n";
    }
  }

  if (showImage) {
    cv::Mat input = results.detected.processed.color.clone();
    // cv::Mat input = results.detected.processed.gray.clone();
    // cv::Mat input = results.detected.processed.binary.clone();
    for (const auto& medal : detected.medals) {
      auto color = cv::Scalar(0, 0, 255);
      cv::circle(input, medal.first, static_cast<int>(medal.second), color, 2);
    }

    if (!detected.outcome.empty()) {
      auto color = cv::Scalar(255, 0, 0);
      cv::rectangle(input, detected.outcome, color, 2);
    }

    cv::namedWindow(filename, cv::WINDOW_NORMAL);
    cv::imshow(filename, input);
  }
}
