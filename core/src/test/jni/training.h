#ifndef OVERWATCHVISION_TRAINING_H
#define OVERWATCHVISION_TRAINING_H

#include "../../main/jni/misc.h"
#include "../../main/jni/results.h"
#include "../../main/jni/stages/recognizeTextMlStage.h"
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

const std::string metadataFileExt = ".metadata";
const std::string defaultMetadata = "default.metadata";

std::vector<std::string>
loadMetadata(const std::string& imageFilename);

bool
validateMetadataCount(const DetectionResults& results,
                      std::vector<std::string> metadata);

void
labelData(const DetectionResults& results,
          const std::string& imageFilename,
          training_data_t& out);

std::vector<std::string>
loadMetadata(const std::string& imageFilename)
{
  auto metadata = std::ifstream(imageFilename + metadataFileExt);
  // Try default (directory-level) metadata file if file-specific one not found.
  if (!metadata.is_open()) {
    const auto dir = imageFilename.substr(0, imageFilename.find_last_of("/"));
    metadata = std::ifstream(dir + "/" + defaultMetadata);
  }
  if (!metadata.is_open()) {
    std::cout << "\tWARN: Could not read metadata for: " << imageFilename
              << "\n";
    return std::vector<std::string>();
  }

  std::string line;
  getline(metadata, line);
  std::stringstream lineStream(line);

  std::vector<std::string> statValues;
  std::string currentStat;
  while (getline(lineStream, currentStat, ',')) {
    statValues.push_back(currentStat);
  }
  return statValues;
}

bool
validateMetadataCount(const DetectionResults& results,
                      std::vector<std::string> metadata)
{
  if (metadata.size() != results.stats.size()) {
    return false;
  }
  for (std::size_t i = 0; i < metadata.size(); i++) {
    if (metadata[i].size() != results.stats[i].size()) {
      return false;
    }
  }
  return true;
}

void
labelData(const DetectionResults& results,
          const std::string& imageFilename,
          training_data_t& out)
{
  const auto metadata = loadMetadata(imageFilename);
  if (!validateMetadataCount(results, metadata)) {
    std::cout << "Metadata != results, skipping\n";
    return;
  }

  // Extract data from pipeline results.
  for (std::size_t i = 0; i < metadata.size(); i++) {
    const auto& value = metadata[i];
    const auto& segments = results.stats[i];
    for (std::size_t j = 0; j < value.size(); j++) {
      out.emplace_back(value[j], segments[j]);
    }
  }
}

#endif // OVERWATCHVISION_TRAINING_H
