cmake_minimum_required(VERSION 3.6.0)
# Android uses clang, so mimic that. Plus gcc errors are horrendous.
set(CMAKE_C_COMPILER "clang")
set(CMAKE_CXX_COMPILER "clang++")
project(core_test)
set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -O3")

# Notice that the jni bindings sources are excluded.
set(SOURCE_FILES
        ../main/jni/stages/binarizeStage.cpp
        ../main/jni/stages/convertInputStage.cpp
        ../main/jni/stages/detectMedalsStage.cpp
        ../main/jni/stages/detectOutcomeStage.cpp
        ../main/jni/stages/detectStatsStage.cpp
        ../main/jni/stages/recognizeTextMlStage.cpp
        ../main/jni/results.cpp
        ../main/jni/misc.cpp
        ../main/jni/pipelineBuilder.cpp
        jni/main.cpp)
add_executable(core_test ${SOURCE_FILES})

set(OpenCV_DIR "/usr/share/opencv")
find_package(OpenCV REQUIRED)
message(STATUS "OpenCV libraries: ${OpenCV_LIBS}")

target_link_libraries(core_test ${OpenCV_LIBS})