#!/bin/bash
# Test utility. Convert jpg files to Android camera frame format (YUV NV21) for sake of testing

ffmpeg -y -i $1 -pix_fmt nv21 ${1%.jpg}.yuv
