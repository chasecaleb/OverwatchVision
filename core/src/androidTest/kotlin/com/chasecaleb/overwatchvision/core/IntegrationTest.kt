package com.chasecaleb.overwatchvision.core

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class IntegrationTest {
    private val testContext = InstrumentationRegistry.getInstrumentation().context
    private val imageNames by lazy {
        val baseDir = "images"
        testContext.assets.list(baseDir)
                .filter { it.endsWith("yuv") }
                .map { "$baseDir/$it" }
                .sorted()
                .also { Assert.assertTrue(it.isNotEmpty()) }
    }

    @Test
    fun test() {
        val detector = FrameDetector(
                rotated = false,
                frameWidth = 1280,
                frameHeight = 960,
                targetWidth = (1280 * 0.85).toInt(),
                context = InstrumentationRegistry.getTargetContext()
        )
        imageNames.forEach {
            val bytes = testContext.assets.open(it).readBytes()
            val results = detector.detect(bytes)
            // Quick/simple happy path test so asserting exact result values doesn't matter.
            // The main point is just that the code returns *something* and doesn't crash
            Assert.assertTrue(results.isValid)
            Assert.assertEquals(results.stats.size, 6)
        }
    }

}