#ifndef OVERWATCHVISION_CHAIN_H
#define OVERWATCHVISION_CHAIN_H

#include <tuple>
#include <utility>

template<typename... Fs>
class Pipeline
{
public:
  Pipeline(Fs&&... fs)
    : functionTuple(std::forward<Fs>(fs)...)
  {}

  template<typename... Ts>
  decltype(auto) operator()(Ts&&... ts)
  {
    return apply(std::integral_constant<size_t, sizeof...(Fs) - 1>{},
                 std::forward<Ts>(ts)...);
  }

private:
  template<size_t N, typename... Ts>
  decltype(auto) apply(std::integral_constant<size_t, N>, Ts&&... ts)
  {
    return apply(std::integral_constant<size_t, N - 1>{},
                 std::get<N>(functionTuple)(std::forward<Ts>(ts)...));
  }

  template<typename... Ts>
  decltype(auto) apply(std::integral_constant<size_t, 0>, Ts&&... ts)
  {
    return std::get<0>(functionTuple)(std::forward<Ts>(ts)...);
  }

  std::tuple<Fs...> functionTuple;
};

/**
 * Composes function objects (stages) together into a pipeline, a la Haskell's .
 * (dot) operator. Functions are applied right to left.
 *
 * @tparam Fs
 * @param fs
 * @return
 */
template<typename... Fs>
decltype(auto)
composeStages(Fs&&... fs)
{
  return Pipeline<Fs...>(std::forward<Fs>(fs)...);
}

template<typename T, typename R>
class PipelineStage
{
public:
  virtual const R operator()(const T& input) = 0;
};

#endif // OVERWATCHVISION_CHAIN_H
