#include "misc.h"
#include <opencv2/opencv.hpp>

std::vector<std::vector<int>>
generateCombinations(unsigned long n, unsigned long k)
{
  if (k > n) {
    return std::vector<std::vector<int>>();
  }

  std::vector<std::size_t> bitmask(n);
  for (std::size_t i = 0; i < k; i++) {
    bitmask[i] = 1;
  }
  for (std::size_t i = k; i < n; i++) {
    bitmask[i] = 0;
  }

  std::vector<std::vector<int>> result;
  do {
    std::vector<int> current;
    for (std::size_t i = 0; i < n; i++) {
      if (bitmask[i]) {
        current.push_back(static_cast<int>(i));
      }
    }
    result.push_back(current);
  } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
  return result;
}

cv::RotatedRect
adjustedRotatedRect(std::vector<cv::Point> points)
{
  auto rect = cv::minAreaRect(points);
  if (rect.angle < -45) {
    rect.angle += 90;
    std::swap(rect.size.width, rect.size.height);
  }
  return rect;
}

cv::Mat
makeSquareBorder(const cv::Mat& original)
{
  const int sideDimen = std::max(original.rows, original.cols);
  cv::Mat squareDigit(sideDimen, sideDimen, original.type(), cv::Scalar(0));

  const cv::Point offset((sideDimen - original.cols) / 2,
                         (sideDimen - original.rows) / 2);
  const auto roiBox = cv::Rect(offset, original.size());
  cv::Mat destRoi(squareDigit, roiBox);
  destRoi += original;
  return squareDigit;
}

cv::Mat
deskew(const cv::Mat& original)
{
  // Determine angle based on image (shape/) moment.
  // This might be too noise-sensitive, but it seems to be fine for now.
  const auto m = cv::moments(original, true);
  const auto angle = m.mu11 / m.mu02;
  // Don't bother rotating if too small of an angle.
  if (std::abs(angle) < 1 / 180 * CV_PI) {
    return original.clone();
  }

  const auto center = cv::Point(original.cols / 2, original.rows / 2);
  const auto matrix = cv::getRotationMatrix2D(center, angle, 1.0);
  cv::Mat deskewed;
  cv::warpAffine(original,
                 deskewed,
                 matrix,
                 original.size(),
                 cv::WARP_INVERSE_MAP | cv::INTER_LINEAR);
  return deskewed;
}