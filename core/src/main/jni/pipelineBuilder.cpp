#include "pipelineBuilder.h"
#include "stages/binarizeStage.h"
#include "stages/convertInputStage.h"
#include "stages/detectMedalsStage.h"
#include "stages/detectOutcomeStage.h"
#include "stages/detectStatsStage.h"
#include "stages/recognizeTextMlStage.h"
#include <opencv2/opencv.hpp>

std::function<RecognitionResults(cv::Mat)>
buildPipeline(double scaleFactor,
              const InputParams& inputParams,
              const std::string& statModelFile)
{
  auto scale = [&](int i) { return static_cast<int>(i * scaleFactor); };

  BinarizeParams binarizeParams = { cv::Size(3, 3), 11, 4 };
  DetectMedalsParams medalParams = { scale(35), scale(180) };

  DetectStatsParams statsParams;
  statsParams.statSize = cv::Size(130, 65);
  statsParams.medalToStatDist = scale(80);
  statsParams.minDigitArea = scale(150);
  statsParams.minDigitHeight = scale(20);

  DetectOutcomeParams outcomeParams = { cv::getStructuringElement(
                                          cv::MORPH_RECT, cv::Size(1, 3)),
                                        scale(250),
                                        scale(35),
                                        scale(650),
                                        210.0 / 620.0 };
  RecognizeTextMlParams textParams = { statModelFile };

  return std::function<RecognitionResults(cv::Mat)>(
    composeStages(RecognizeTextMlStage(textParams),
                  DetectOutcomeStage(outcomeParams),
                  DetectStatsStage(statsParams),
                  DetectMedalsStage(medalParams),
                  BinarizeStage(binarizeParams),
                  ConvertInputStage(inputParams)));
}
