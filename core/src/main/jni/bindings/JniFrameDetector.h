#ifndef OVERWATCHVISION_JNIFRAMEDETECTOR_H
#define OVERWATCHVISION_JNIFRAMEDETECTOR_H

#include "../pipeline.h"
#include "../results.h"
#include "jni.h"
#include <functional>
#include <opencv2/core/mat.hpp>

/**
 * Cache for JNI lookup info.
 */
struct JniCache
{
  jclass stringClass;
  jmethodID resultsSetStats;
};

/**
 * Native side of JNI border. Implements native methods of Kotlin FrameDetector
 * class.
 */
class JniFrameDetector
{
  std::function<RecognitionResults(cv::Mat)> pipeline;
  const JniCache cache;
  const int frameWidth, frameHeight;
  cv::Mat buffer;

public:
  static void doClose(JNIEnv* env, jobject jvmSelf);

  static void initialize(JNIEnv* env,
                         jobject jvmSelf,
                         jint frameWidth,
                         jint frameHeight,
                         jboolean rotated,
                         jint targetWidth,
                         jstring modelFileRef);

  static void doDetect(JNIEnv* env,
                       jobject jvmSelf,
                       jbyteArray nv21Frame,
                       jobject jvmResults);

  JniFrameDetector(JniCache cache,
                   std::function<RecognitionResults(cv::Mat)> pipeline,
                   int frameWidth,
                   int frameHeight);

private:
  static JniCache initializeCache(JNIEnv* env);

  static void logFailure(const RecognitionResults& results);
};

#endif // OVERWATCHVISION_JNIFRAMEDETECTOR_H
