#ifndef OVERWATCHVISION_JNIUTILS_H
#define OVERWATCHVISION_JNIUTILS_H

#include <jni.h>

namespace {
jfieldID
getHandleField(JNIEnv* env, jobject obj)
{
  jclass c = env->GetObjectClass(obj);
  // J = long.
  return env->GetFieldID(c, "nativeHandle", "J");
}
} // namespace

template<typename T>
T*
getHandle(JNIEnv* env, jobject obj)
{
  jlong handle = env->GetLongField(obj, getHandleField(env, obj));
  return reinterpret_cast<T*>(handle);
}

template<typename T>
void
setHandle(JNIEnv* env, jobject obj, T* t)
{
  jlong handle = reinterpret_cast<jlong>(t);
  env->SetLongField(obj, getHandleField(env, obj), handle);
}

#endif // OVERWATCHVISION_JNIUTILS_H
