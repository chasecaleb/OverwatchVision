#include "JniFrameDetector.h"
#include "../pipelineBuilder.h"
#include "../stages/binarizeStage.h"
#include "../stages/convertInputStage.h"
#include "../stages/detectMedalsStage.h"
#include "../stages/detectOutcomeStage.h"
#include "../stages/detectStatsStage.h"
#include "jniUtils.h"
#include <android/log.h>
#include <opencv2/opencv.hpp>
#include <string>

void
JniFrameDetector::doClose(JNIEnv* env, jobject jvmSelf)
{
  auto* nativeObj = getHandle<JniFrameDetector>(env, jvmSelf);
  env->DeleteGlobalRef(nativeObj->cache.stringClass);
  delete nativeObj;
}

JniFrameDetector::JniFrameDetector(
  JniCache cache,
  std::function<RecognitionResults(cv::Mat)> pipeline,
  int frameWidth,
  int frameHeight)
  : pipeline(pipeline)
  , cache(cache)
  , frameWidth(frameWidth)
  , frameHeight(frameHeight)
{}

void
JniFrameDetector::initialize(JNIEnv* env,
                             jobject jvmSelf,
                             jint frameWidth,
                             jint frameHeight,
                             jboolean rotated,
                             jint targetWidth,
                             jstring modelFileRef)
{
  JniCache jniCache = initializeCache(env);
  if (env->ExceptionOccurred()) {
    return;
  }

  const double scaleFactor = targetWidth / (double)frameWidth;
  const auto inputParams = InputParams{ (bool)rotated, cv::COLOR_YUV2BGR_NV21 };

  const auto modelFile = env->GetStringUTFChars(modelFileRef, nullptr);
  auto pipeline = buildPipeline(scaleFactor, inputParams, modelFile);
  env->ReleaseStringUTFChars(modelFileRef, modelFile);
  auto* self =
    new JniFrameDetector(jniCache, pipeline, frameWidth, frameHeight);
  setHandle<JniFrameDetector>(env, jvmSelf, self);
}

void
JniFrameDetector::doDetect(JNIEnv* env,
                           jobject jvmSelf,
                           jbyteArray nv21Frame,
                           jobject jvmResults)
{
  auto* self = getHandle<JniFrameDetector>(env, jvmSelf);

  uint8_t* frame = (uint8_t*)env->GetByteArrayElements(nv21Frame, nullptr);
  if (frame == nullptr || env->ExceptionOccurred()) {
    return;
  }
  // Dimensions and type parameter based on how OpenCV does it in
  // JavaCameraView. I think this is because YUV/NV21 is single-channel
  // interleaved. Point being: this is based on the reference implementation and
  // it works.
  cv::Mat frameMat(self->frameHeight + self->frameHeight / 2,
                   self->frameWidth,
                   CV_8UC1,
                   frame);
  const auto results = self->pipeline(frameMat);
  env->ReleaseByteArrayElements(nv21Frame, (jbyte*)frame, JNI_ABORT);
  if (!results.isValid()) {
    logFailure(results);
    return;
  }

  const std::size_t size = results.stats.size();
  std::vector<int> tmpArray(size);
  for (std::size_t i = 0; i < size; i++) {
    tmpArray[i] = std::atoi(results.stats[i].c_str());
  }
  auto jvmStats = env->NewIntArray((jsize)size);
  if (jvmStats == nullptr) {
    return;
  }
  env->SetIntArrayRegion(jvmStats, (jsize)0, (jsize)size, tmpArray.data());
  env->CallVoidMethod(jvmResults, self->cache.resultsSetStats, jvmStats);
  env->ReleaseIntArrayElements(jvmStats, tmpArray.data(), JNI_COMMIT);
}

void
JniFrameDetector::logFailure(const RecognitionResults& results)
{
  const auto doLog = [](const char* fmt, auto... args) {
    __android_log_print(ANDROID_LOG_VERBOSE, "JniFrameDetector", fmt, args...);
  };
  const auto& detected = results.detected;
  if (!detected.isValid()) {
    doLog("Detection stage failed (medals=%zu, stats=%zu, outcome=%s)",
          detected.medals.size(),
          detected.stats.size(),
          detected.outcome.empty() ? "false" : "true");
  } else {
    doLog("Recognition stage failed", "");
  }
}

JniCache
JniFrameDetector::initializeCache(JNIEnv* env)
{
  const auto stringClass =
    env->NewGlobalRef(env->FindClass("java/lang/String"));

  const auto resClass =
    env->FindClass("com/chasecaleb/overwatchvision/core/DetectionResults");
  if (env->ExceptionOccurred()) {
    return JniCache();
  }
  const auto setStats = env->GetMethodID(resClass, "setStats", "([I)V");
  if (env->ExceptionOccurred()) {
    return JniCache();
  }

  return JniCache{ (jclass)stringClass, setStats };
}
