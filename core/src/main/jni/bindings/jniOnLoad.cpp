#include "JniFrameDetector.h"
#include <android/log.h>
#include <jni.h>
#include <map>
#include <string>
#include <vector>

static const auto LOG_TAG = "jniOnLoad";

static const std::string BASE_PACKAGE = "com/chasecaleb/overwatchvision/core/";
static const std::map<std::string, std::vector<JNINativeMethod>> JNI_INFO{
  { BASE_PACKAGE + "FrameDetector",
    { { "doClose", "()V", (void*)JniFrameDetector::doClose },
      { "initialize",
        "(IIZILjava/lang/String;)V",
        (void*)JniFrameDetector::initialize },
      { "doDetect",
        "([BLcom/chasecaleb/overwatchvision/core/DetectionResults;)V",
        (void*)JniFrameDetector::doDetect } } },
};

/**
 * Magic method called by JVM on library load for initialization.
 *
 * @param vm
 * @return
 */
jint
JNI_OnLoad(JavaVM* vm, void*)
{
  JNIEnv* env;
  if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "Getting env failed");
    return JNI_ERR;
  }

  for (auto& info : JNI_INFO) {
    auto clazz = env->FindClass(info.first.c_str());
    if (!clazz) {
      __android_log_print(ANDROID_LOG_FATAL,
                          LOG_TAG,
                          "Could not find JNI class: %s",
                          info.first.c_str());
      return JNI_ERR;
    }
    env->RegisterNatives(clazz, &info.second[0], (jint)info.second.size());
    __android_log_print(ANDROID_LOG_DEBUG,
                        LOG_TAG,
                        "Registered JNI class: %s",
                        info.first.c_str());
  }

  __android_log_print(
    ANDROID_LOG_DEBUG, LOG_TAG, "OnLoad finished successfully");
  return JNI_VERSION_1_6;
}