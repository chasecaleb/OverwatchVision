#include "binarizeStage.h"
#include "../results.h"
#include "convertInputStage.h"
#include <opencv2/opencv.hpp>

BinarizeStage::BinarizeStage(const BinarizeParams params)
  : params(params)
{}

const PreProcessResults
BinarizeStage::operator()(const cv::Mat& input)
{
  // Unsharp mask to sharpen edges and remove noise.
  // This is especially important to prevent contours in close proximity from
  // touching. A couple iterations of bilateral filter might be better, but this
  // is faster.
  cv::GaussianBlur(input, blurred, params.blurKernel, 0);

  // Threshold (binarize).
  cv::cvtColor(blurred, gray, cv::COLOR_BGR2GRAY);
  cv::adaptiveThreshold(gray,
                        binary,
                        255,
                        cv::ADAPTIVE_THRESH_GAUSSIAN_C,
                        cv::THRESH_BINARY_INV,
                        params.thresholdSize,
                        params.thresholdConstant);
  return PreProcessResults{ blurred, gray, binary };
}