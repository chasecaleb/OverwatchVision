#ifndef OVERWATCHVISION_MLTEXTRECOGNITIONSTAGE_H
#define OVERWATCHVISION_MLTEXTRECOGNITIONSTAGE_H

#include "../pipeline.h"
#include "../results.h"
#include <opencv2/ml.hpp>

struct RecognizeTextMlParams
{
  std::string trainedModelFile;
};

/**
 * Recognizes value of individual stat digits using machine-learning approach.
 */
class RecognizeTextMlStage
  : public PipelineStage<DetectionResults, RecognitionResults>
{
  const RecognizeTextMlParams params;
  const cv::Ptr<cv::ml::SVM> svm;

public:
  /**
   * Initializes and trains a new classification model.
   *
   * @param data
   * @return
   */
  static cv::Ptr<cv::ml::StatModel> trainModel(const training_data_t& data);

  RecognizeTextMlStage(RecognizeTextMlParams params);

  const RecognitionResults operator()(const DetectionResults& input) override;

private:
  static cv::Mat extractFeatures(const cv::Mat& digit);
};

#endif // OVERWATCHVISION_MLTEXTRECOGNITIONSTAGE_H
