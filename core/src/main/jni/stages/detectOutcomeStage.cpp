#include "detectOutcomeStage.h"
#include "../misc.h"

DetectOutcomeStage::DetectOutcomeStage(const DetectOutcomeParams params)
  : params(params)
{}

const DetectionResults
DetectOutcomeStage::operator()(const DetectionResults& input)
{
  // If stats not found, then there's no point trying to find outcome.
  if (input.stats.empty()) {
    return input;
  }

  auto contours = doFindContours(input.processed.binary);
  auto centroid = findMedalCentroid(input);
  auto outcome = findOutcome(contours, centroid, input.scaleFactor);

  auto results = input;
  results.outcome = outcome;
  return results;
}

contours_t
DetectOutcomeStage::doFindContours(const cv::Mat& input)
{
  cv::morphologyEx(input, transformBuffer, cv::MORPH_CLOSE, params.morphKernel);
  contours_t contours;
  cv::findContours(
    transformBuffer, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
  return contours;
}

cv::Point
DetectOutcomeStage::findMedalCentroid(const DetectionResults& input) const
{
  int sumX = 0;
  int sumY = 0;
  for (const auto& medal : input.medals) {
    sumX += medal.first.x;
    sumY += medal.first.y;
  }
  return cv::Point((int)(sumX / input.medals.size()),
                   (int)(sumY / input.medals.size()));
}

cv::Rect2f
DetectOutcomeStage::findOutcome(
  const std::vector<std::vector<cv::Point>>& contours,
  const cv::Point& medalsCentroid,
  double scale) const
{
  cv::Rect2f bestRect = cv::Rect2f();
  double bestScore = std::numeric_limits<double>::max();
  for (const auto& contour : contours) {
    std::vector<cv::Point> approximated;
    cv::approxPolyDP(
      contour, approximated, 0.01 * cv::arcLength(contour, true), true);
    const auto rotatedRect = adjustedRotatedRect(approximated);

    auto widthError =
      std::abs(rotatedRect.size.width - params.outcomeWidth * scale);
    auto heightError =
      std::abs(rotatedRect.size.height - params.outcomeHeight * scale);
    if (widthError > params.widthTolerance ||
        heightError > params.heightTolerance) {
      continue;
    }

    // Outcome should be in top-left of screen quadrant of screen.
    if (rotatedRect.center.y > medalsCentroid.y ||
        rotatedRect.center.x > medalsCentroid.x) {
      continue;
    }

    auto dist = cv::norm(medalsCentroid - rotatedRect.boundingRect().tl());
    auto distError = std::abs(dist - params.medalDist * scale);
    auto slope = (medalsCentroid.y - rotatedRect.center.y) /
                 (double)(medalsCentroid.x - rotatedRect.center.x);
    auto slopeError = std::abs(slope - params.medalSlope);
    if (distError > params.distTolerance ||
        slopeError > params.slopeTolerance) {
      continue;
    }

    auto score =
      distError / params.distTolerance + slopeError / params.slopeTolerance;
    if (score < bestScore) {
      bestScore = score;
      bestRect = rotatedRect.boundingRect2f();
    }
  }
  return bestRect;
}
