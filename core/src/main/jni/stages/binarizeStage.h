#ifndef OVERWATCHVISION_BINARIZESTAGE_H
#define OVERWATCHVISION_BINARIZESTAGE_H

#include "../pipeline.h"
#include "../results.h"
#include <opencv2/core/mat.hpp>

struct BinarizeParams
{
  cv::Size blurKernel;
  int thresholdSize, thresholdConstant;
};

/**
 * Preprocessing stage: takes color (BGR) input, blurs/cleans up, and binarizes.
 */
class BinarizeStage : public PipelineStage<cv::Mat, PreProcessResults>
{
public:
  BinarizeStage(const BinarizeParams params);

  const PreProcessResults operator()(const cv::Mat& input) override;

private:
  const BinarizeParams params;
  cv::Mat blurred, blurredTmp, gray, binary;
};
#endif // OVERWATCHVISION_BINARIZESTAGE_H
