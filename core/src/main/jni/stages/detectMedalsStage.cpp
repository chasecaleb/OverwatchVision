#include "detectMedalsStage.h"
#include "../misc.h"
#include <numeric>
#include <opencv2/opencv.hpp>

const DetectionResults
DetectMedalsStage::operator()(const PreProcessResults& input)
{
  auto medals = findMedals(input.gray);
  filterMedals(medals);
  // Scale factor from idealized dimensions to measured ones to aid later
  // stages.
  double scaleFactor;
  if (!medals.empty()) {
    const auto medalWidth =
      cv::norm(medals[0].first - medals[medals.size() - 1].first);
    // Multiply individual spacing by five because it's the distance between 1st
    // and 6th.
    scaleFactor = medalWidth / (params.medalSpacing * 5);
  } else {
    scaleFactor = 1.0;
  }
  return DetectionResults{ input, scaleFactor, medals };
}

medals_t
DetectMedalsStage::findMedals(cv::Mat input)
{
  std::vector<cv::Vec3f> circles;
  const int minRadius = (int)(params.medalRadius - params.radiusTolerance);
  const int maxRadius = (int)(params.medalRadius + params.radiusTolerance);
  const auto rawThreshold = cv::mean(input)[0];
  // Constrain canny threshold to something at least remotely sane.
  const auto threshold = std::min(225.0, std::max(10.0, rawThreshold));
  // TODO: tune hough circle performance (~50% of pipeline time on desktop)
  //  - constrain to subimage roi (with healthy margin) - should be about 1/3rd
  //  of frame.
  //  - (maybe) increase accumulator threshold (25->50?)

  cv::HoughCircles(input,
                   circles,
                   cv::HOUGH_GRADIENT,
                   1,
                   params.medalSpacing / 2,
                   threshold,
                   25,
                   minRadius,
                   maxRadius);

  medals_t results;
  // Returned circles are sorted by strongest first, so limit to a reasonable
  // number.
  const auto last = std::min<std::size_t>(circles.size(), 12);
  for (std::size_t i = 0; i < last; i++) {
    const auto& c = circles[i];
    results.emplace_back(cv::Point((int)c[0], (int)c[1]), c[2]);
  }
  return results;
}

void
DetectMedalsStage::filterMedals(medals_t& medals)
{
  // Find best/most likely match for medals. Selection criteria:
  //  - Should be horizontally aligned (so same y coord, with tolerance for
  //  rotation).
  //  - Should be equally spaced apart by expected distance.
  //  - TODO: Check that they're at roughly the expected absolute position as
  //  well?
  std::sort(medals.begin(), medals.end(), [](auto a, auto b) {
    return a.first.x < b.first.x;
  });

  auto scorer = [&](std::vector<std::pair<cv::Point2f, float>>& combination) {
    // Medal centerpoints should be collinear, in which case rect will be very
    // skinny.
    std::vector<cv::Point2f> centerPoints(combination.size());
    std::transform(combination.begin(),
                   combination.end(),
                   centerPoints.begin(),
                   [&](auto it) { return it.first; });
    auto rect = cv::minAreaRect(centerPoints);
    auto lineError = std::min(rect.size.width, rect.size.height);
    double invalidScore = std::numeric_limits<double>::max();
    if (lineError > params.alignmentTolerance) {
      return invalidScore;
    }

    // Adjacent medals should be equally spaced apart by known amount.
    std::vector<double> dists(combination.size() - 1);
    auto validDists = true;
    for (std::size_t i = 0; i < dists.size(); i++) {
      dists[i] = cv::norm(centerPoints[i] - centerPoints[i + 1]);
      double spacingError = std::abs(dists[i] - params.medalSpacing);
      if (spacingError > params.spacingTolerance) {
        validDists = false;
        break;
      }
    }
    if (!validDists) {
      return invalidScore;
    }
    auto avgDist =
      std::accumulate(dists.begin(), dists.end(), 0.0) / dists.size();
    auto distError =
      std::accumulate(dists.begin(), dists.end(), 0.0, [&](auto acc, auto d) {
        return acc + std::abs(avgDist - d);
      });
    return lineError / params.radiusTolerance +
           distError / params.spacingTolerance;
  };
  medals = findBestCombination(medals, 6, scorer);
}

DetectMedalsStage::DetectMedalsStage(const DetectMedalsParams params)
  : params(params)
{}
