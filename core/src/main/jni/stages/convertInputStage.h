#ifndef OVERWATCHVISION_CONVERTINPUTSTAGE_H
#define OVERWATCHVISION_CONVERTINPUTSTAGE_H

#include "../pipeline.h"
#include <opencv2/opencv.hpp>

struct InputParams
{
  bool inputIsRotated;
  /**
   * Conversion code for input. Output should be BGR.
   */
  cv::ColorConversionCodes format;
};

/**
 * Converts external image to OpenCV format.
 */
class ConvertInputStage : public PipelineStage<cv::Mat, cv::Mat>
{
  const InputParams params;
  cv::Mat rotated, scaledYuv, scaledBgr;

public:
  ConvertInputStage(const InputParams params);

  const cv::Mat operator()(const cv::Mat& input) override;
};

#endif // OVERWATCHVISION_CONVERTINPUTSTAGE_H
