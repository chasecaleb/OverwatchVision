#include "convertInputStage.h"

ConvertInputStage::ConvertInputStage(const InputParams params)
  : params(params)
{}

const cv::Mat
ConvertInputStage::operator()(const cv::Mat& input)
{
  cv::Mat bgr, scaled;
  cv::cvtColor(input, bgr, params.format, 3);
  auto scaleSize =
    cv::Size(1280, static_cast<int>(1280.0 / bgr.cols * bgr.rows));
  cv::resize(bgr, scaled, scaleSize);
  if (params.inputIsRotated) {
    cv::Mat out;
    cv::flip(scaled, out, -1);
    return out;
  } else {
    return scaled;
  }
}