#include "detectStatsStage.h"
#include "../misc.h"
#include <numeric>
#include <opencv2/opencv.hpp>

const int HIERARCHY_NEXT_IDX = 0;
const int HIERARCHY_CHILD_IDX = 2;
const int HIERARCHY_EMPTY_VALUE = -1;

DetectStatsStage::DetectStatsStage(const DetectStatsParams params)
  : params(params)
{}

const DetectionResults
DetectStatsStage::operator()(const DetectionResults& input)
{
  // If medals aren't found, then there's no point trying to find stats.
  if (input.medals.empty()) {
    return input;
  }
  const auto statOutlines =
    input.calculateStatOutlines(params.medalToStatDist, params.statSize);
  // Couldn't determine stat outlines - abort.
  if (statOutlines.size() != input.medals.size()) {
    return input;
  }

  stats_t stats;
  for (const auto& outline : statOutlines) {
    const cv::Mat roi(input.processed.binary, outline.boundingRect());

    contours_t allContours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(roi,
                     allContours,
                     hierarchy,
                     cv::RETR_TREE,
                     cv::CHAIN_APPROX_SIMPLE,
                     outline.boundingRect().tl());
    auto contourInfo = findCandidates(outline, allContours, hierarchy);
    filterByHeight(contourInfo);

    std::vector<cv::Mat> digits;
    for (const auto& info : contourInfo) {
      extractDigit(info, allContours, hierarchy, digits);
    }
    stats.push_back(digits);
  }

  auto results = input;
  results.stats = stats;
  return results;
}

void
DetectStatsStage::extractDigit(
  const std::pair<size_t, cv::RotatedRect>& contourInfo,
  const contours_t& allContours,
  const std::vector<cv::Vec4i>& hierarchy,
  std::vector<cv::Mat>& out) const
{
  const auto ctrIdx = contourInfo.first;
  auto holeCtrs = getChildren(ctrIdx, allContours, hierarchy);
  // Filter small holes, which are just insignificant noise
  const auto minHoleArea = 0.02 * contourInfo.second.size.area();
  erase_where(holeCtrs, [&](const auto& c) {
    return cv::boundingRect(c).size().area() < minHoleArea;
  });

  // TODO: refactor this nested atrocity.
  // TODO: handle >2 overlapping digits?
  // Theoretically, three least significant digits of damage/healing done could
  // overlap. Three should be max, since comma for thousands should be enough of
  // a gap.
  const auto& contour = allContours[ctrIdx];
  if (!hasOverlap(contourInfo)) {
    out.emplace_back(doDrawContours(contour, holeCtrs));
    return;
  }
  const auto overlapIdxs = findOverlap(contour);
  const auto splitCtrs = splitContour(contour, overlapIdxs);
  for (const auto& s : splitCtrs) {
    contours_t sHoles;
    for (const auto& h : holeCtrs) {
      std::vector<cv::Point> filtered;
      for (const auto& hPt : h) {
        if (pointPolygonTest(s, hPt, false) > 0) {
          filtered.push_back(hPt);
        }
      }
      // OpenCV will crash on attempting to draw empty contour (plus it wouldn't
      // make sense).
      if (!filtered.empty()) {
        sHoles.push_back(filtered);
      }
    }
    out.emplace_back(doDrawContours(s, sHoles));
  }
}

/**
 * Determines if contour contains multiple overlapping/touching digits.
 *
 * @param info
 * @return
 */
bool
DetectStatsStage::hasOverlap(
  const std::pair<size_t, cv::RotatedRect>& info) const
{
  const auto size = info.second.size;
  const double ratio = size.width / (double)size.height;
  bool hasOverlap = ratio > params.maxWidthRatio;
  return hasOverlap;
}

/**
 * Return most likely intersection point of two overlapping objects based on
 * convexity defects.
 *
 * @param info
 * @param allContours
 * @return Pair of point indices. First index will be lowest. Will both be -1 if
 * not found.
 */
std::pair<int, int>
DetectStatsStage::findOverlap(const std::vector<cv::Point>& contour) const
{
  auto defectIdxs = [&]() {
    std::vector<int> hull;
    cv::convexHull(contour, hull);
    std::vector<cv::Vec4i> defects;
    cv::convexityDefects(contour, hull, defects);
    std::vector<int> idxs;
    for (const auto& d : defects) {
      idxs.push_back(d[2]);
    }
    return idxs;
  }();

  // Return closest two points, which most likely define the top + bottom of
  // intersection. Default (-1's) means none.
  std::pair<int, int> bestPair = std::make_pair(-1, -1);
  double bestDist = std::numeric_limits<double>::max();
  for (const auto i : defectIdxs) {
    for (const auto j : defectIdxs) {
      if (i == j) {
        continue;
      }
      const auto ptI = contour[i];
      const auto ptJ = contour[j];
      // Penalize x distance more because the two points should be
      // vertically-ish stacked.
      const double dist = std::abs(ptI.x - ptJ.x) * 2 + std::abs(ptI.y - ptJ.y);
      if (dist < bestDist) {
        bestDist = dist;
        bestPair = std::make_pair(i, j);
      }
    }
  }

  return std::minmax(bestPair.first, bestPair.second);
}

/**
 * Split single contour into two at overlap point.
 *
 * @param contour
 * @param overlapIdxs
 * @return
 */
contours_t
DetectStatsStage::splitContour(const std::vector<cv::Point>& contour,
                               const std::pair<int, int> overlapIdxs) const
{
  // Overlap point not found, so fallback to treating as single contour.
  if (overlapIdxs.first == -1) {
    return { contour };
  }
  std::vector<cv::Point> a, b;
  for (std::size_t i = 0; i < contour.size(); i++) {
    if ((int)i >= overlapIdxs.first && (int)i < overlapIdxs.second) {
      a.emplace_back(contour[i]);
    } else {
      b.emplace_back(contour[i]);
    }
  }

  // Don't allow split to create impossible digits - abort.
  if (!isDigit(a, adjustedRotatedRect(a)) ||
      !isDigit(b, adjustedRotatedRect(b))) {
    return { contour };
  }

  // Sort by x position to avoid transposed digits
  const auto findMin = [](const cv::Point& lhs, const cv::Point& rhs) {
    return lhs.x < rhs.x;
  };
  const auto aMin = *std::min_element(a.begin(), a.end(), findMin);
  const auto bMin = *std::min_element(b.begin(), b.end(), findMin);
  if (aMin.x > bMin.x) {
    std::swap(a, b);
  }

  return { a, b };
}

void
DetectStatsStage::filterByHeight(
  std::vector<std::pair<size_t, cv::RotatedRect>>& contourInfo) const
{
  // Digits should all be almost exactly the same height - others are
  // noise/punctuation.
  double heightSum = 0;
  for (const auto& info : contourInfo) {
    heightSum += info.second.boundingRect().height;
  }
  const auto avgHeight = heightSum / contourInfo.size();
  const auto heightTolerance = avgHeight * params.heightTolerance;
  erase_where(contourInfo, [&](auto it) {
    return std::abs(it.second.boundingRect().height - avgHeight) >
           heightTolerance;
  });
}

std::vector<std::pair<size_t, cv::RotatedRect>>
DetectStatsStage::findCandidates(const cv::RotatedRect& outline,
                                 const contours_t& allContours,
                                 const std::vector<cv::Vec4i>& hierarchy) const
{
  std::vector<std::pair<std::size_t, cv::RotatedRect>> contourInfo;
  for (std::size_t i = 0; i < allContours.size(); i++) {
    if (isDigit(allContours[i], outline)) {
      const auto holes = getChildren(i, allContours, hierarchy);
      const auto validChild =
        any_of(holes.begin(), holes.end(), [&](const auto& h) {
          return isDigit(h, outline);
        });
      if (validChild) {
        continue;
      }
      contourInfo.emplace_back(i, adjustedRotatedRect(allContours[i]));
    }
  }

  sort(contourInfo.begin(),
       contourInfo.end(),
       [&](const auto& lhs, const auto& rhs) {
         return lhs.second.center.x < rhs.second.center.x;
       });
  return contourInfo;
}

cv::Mat
DetectStatsStage::doDrawContours(const std::vector<cv::Point>& exterior,
                                 const contours_t& holes) const
{
  const auto bounds = cv::boundingRect(exterior);
  cv::Mat drawn = cv::Mat::zeros(bounds.size(), CV_8UC1);

  const auto doDraw = [&](const contours_t& ctrs, const int color) {
    const cv::Point2i& offset =
      cv::Point(-1 * bounds.tl().x, -1 * bounds.tl().y);
    cv::drawContours(drawn,
                     ctrs,
                     -1,
                     cv::Scalar(color),
                     -1,
                     cv::LINE_8,
                     cv::noArray(),
                     std::numeric_limits<int>::max(),
                     offset);
  };
  contours_t tmpOutters;
  tmpOutters.push_back(exterior);
  doDraw(tmpOutters, 255);
  doDraw(holes, 0);
  return drawn;
}

bool
DetectStatsStage::isDigit(const std::vector<cv::Point>& contour,
                          const cv::RotatedRect& outline) const
{
  const auto contourRect = adjustedRotatedRect(contour);
  std::vector<cv::Point> ignored;
  const auto& intersectType =
    cv::rotatedRectangleIntersection(outline, contourRect, ignored);

  const bool validPosition =
    intersectType == cv::RectanglesIntersectTypes::INTERSECT_FULL;
  const bool validArea = contourRect.size.area() > params.minDigitArea;
  const bool validHeight = contourRect.size.height > params.minDigitHeight;
  return validPosition && validArea && validHeight;
}

contours_t
DetectStatsStage::getChildren(std::size_t parentIdx,
                              const contours_t& contours,
                              const std::vector<cv::Vec4i>& hierarchy) const
{
  contours_t result;
  auto nextChildIdx = hierarchy[parentIdx][HIERARCHY_CHILD_IDX];
  while (nextChildIdx != HIERARCHY_EMPTY_VALUE) {
    result.push_back(contours[nextChildIdx]);
    nextChildIdx = hierarchy[nextChildIdx][HIERARCHY_NEXT_IDX];
  }
  return result;
}
