#ifndef OVERWATCHVISION_OUTCOMEDETECTIONSTAGE_H
#define OVERWATCHVISION_OUTCOMEDETECTIONSTAGE_H

#include "../pipeline.h"
#include "../results.h"
#include <opencv2/opencv.hpp>

struct DetectOutcomeParams
{
  const cv::Mat morphKernel;
  const int outcomeWidth, outcomeHeight;
  // Location of medals centroid relative to top-left corner of outcome bounding
  // box. (Since there are six medals, the centroid should be in between 3rd and
  // 4th).
  const int medalDist;
  const double medalSlope;
  const double widthTolerance = outcomeWidth * 0.6;
  const double heightTolerance = outcomeHeight * 0.6;
  // Slope conversion refresher: 1 degree = 0.0175 slope, 5 degrees = 0.0875
  // slope
  const double slopeTolerance = 0.10;
  const double distTolerance = medalDist * 0.1;
};

/**
 * Detects position of match outcome (i.e. victory/defeat/draw).
 */
class DetectOutcomeStage
  : public PipelineStage<DetectionResults, DetectionResults>
{
  const DetectOutcomeParams params;
  cv::Mat transformBuffer;

public:
  DetectOutcomeStage(const DetectOutcomeParams params);

  const DetectionResults operator()(const DetectionResults& input) override;

private:
  std::vector<std::vector<cv::Point>> doFindContours(const cv::Mat& input);

  cv::Rect2f findOutcome(const std::vector<std::vector<cv::Point>>& contours,
                         const cv::Point& medalsCentroid,
                         double scale) const;

  cv::Point findMedalCentroid(const DetectionResults& input) const;
};

#endif // OVERWATCHVISION_OUTCOMEDETECTIONSTAGE_H
