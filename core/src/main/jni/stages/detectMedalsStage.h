#ifndef OVERWATCHVISION_MEDALDETECTIONSTAGE_H
#define OVERWATCHVISION_MEDALDETECTIONSTAGE_H

#include "../pipeline.h"
#include "../results.h"

struct DetectMedalsParams
{
  int medalRadius, medalSpacing;
  double radiusTolerance = medalRadius * 0.4;
  double alignmentTolerance = medalRadius * 0.4;
  double spacingTolerance = medalSpacing * 0.4;
};

/**
 * Detects position of stat medals (including placeholders for medals that
 * weren't awarded).
 */
class DetectMedalsStage
  : public PipelineStage<PreProcessResults, DetectionResults>
{
  const DetectMedalsParams params;

public:
  DetectMedalsStage(const DetectMedalsParams params);

  const DetectionResults operator()(const PreProcessResults& input) override;

private:
  medals_t findMedals(cv::Mat input);

  void filterMedals(medals_t& medals);
};

#endif // OVERWATCHVISION_MEDALDETECTIONSTAGE_H
