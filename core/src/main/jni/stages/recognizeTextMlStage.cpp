#include "recognizeTextMlStage.h"
#include "../misc.h"

// Constant instead of param because it's needed from static fields and this is
// simplest. There really isn't a strong need to parameterize this anyways,
// after all.
const cv::Size featureSize(64, 64);

/**
 * Initialize classification model.
 *
 * @param filename Either empty string or valid filename. New model will be
 * created if empty, otherwise it will be loaded from file.
 * @return
 */
cv::Ptr<cv::ml::SVM>
initModel(const std::string& filename)
{
  if (filename.empty()) {
    const auto svm = cv::ml::SVM::create();
    svm->setType(cv::ml::SVM::Types::C_SVC);
    return svm;
  } else {
    return cv::Algorithm::load<cv::ml::SVM>(filename);
  }
}

cv::Ptr<cv::ml::StatModel>
RecognizeTextMlStage::trainModel(const training_data_t& data)
{
  // OpenCV training data format: one row per sample in floating-point Mat.
  cv::Mat samples((int)data.size(), 0, CV_32F);
  cv::Mat responses((int)data.size(), 0, CV_32S);
  for (const auto& pair : data) {
    samples.push_back(RecognizeTextMlStage::extractFeatures(pair.second));
    responses.push_back(cv::Mat(1, 1, CV_32S, pair.first));
  }

  const auto& svm = initModel(std::string());
  const auto trainData =
    cv::ml::TrainData::create(samples, cv::ml::ROW_SAMPLE, responses);
  // RBF kernel is theoretically at least as good as linear (given enough
  // samples), but linear accuracy seems fine and is several orders of magnitude
  // faster to train. Plus, there shouldn't be a significant difference since I
  // have 81 features for 10 classes.
  svm->setKernel(cv::ml::SVM::KernelTypes::LINEAR);
  svm->trainAuto(trainData);
  return svm;
}

cv::Mat
RecognizeTextMlStage::extractFeatures(const cv::Mat& digit)
{
  const auto squareMat = makeSquareBorder(digit);
  cv::Mat resized;
  cv::resize(squareMat, resized, featureSize);

  // Opening should theoretically should help with noise, but realistically it
  // may not matter.
  cv::morphologyEx(resized,
                   resized,
                   cv::MORPH_OPEN,
                   cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5)));

  const auto deskewed = deskew(resized);
  cv::HOGDescriptor hog(
    featureSize, featureSize / 2, featureSize / 4, featureSize / 2, 9);
  std::vector<float> descriptors;
  hog.compute(deskewed, descriptors);
  cv::Mat1f out(1, (int)descriptors.size(), descriptors.data());
  // Need to do clone in order to deep-copy data.
  return out.clone();
}

RecognizeTextMlStage::RecognizeTextMlStage(RecognizeTextMlParams params)
  : params(params)
  , svm(initModel(params.trainedModelFile))
{}

const RecognitionResults
RecognizeTextMlStage::operator()(const DetectionResults& input)
{
  std::vector<std::string> stats;
  // Currently don't care about output in top-left corner, so not checking
  // isValid()
  //    if (!input.isValid() || !knn->isTrained()) {
  if (input.stats.empty() || !svm->isTrained()) {
    return RecognitionResults{ input, stats };
  }

  for (const auto& segmentGroup : input.stats) {
    std::string value = "";
    for (const auto& segment : segmentGroup) {
      cv::Mat data = extractFeatures(segment);
      const auto result = svm->predict(data);
      value += (char)result;
    }
    stats.emplace_back(value);
  }

  return RecognitionResults{ input, stats };
}
