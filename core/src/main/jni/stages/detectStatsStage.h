#ifndef OVERWATCHVISION_STATDETECTIONSTAGE_H
#define OVERWATCHVISION_STATDETECTIONSTAGE_H

#include "../pipeline.h"
#include "../results.h"
#include <opencv2/opencv.hpp>
#include <utility>

struct DetectStatsParams
{
  cv::Size statSize;
  int medalToStatDist;
  int minDigitArea;
  int minDigitHeight;
  double heightTolerance = 0.25;
  /** Max ratio of width/height - used to detect merged contours. */
  double maxWidthRatio = 0.55;
};

/**
 * Detects position of stat value digits (i.e. the big numbers right below the
 * medals).
 */
class DetectStatsStage
  : public PipelineStage<DetectionResults, DetectionResults>
{
  const DetectStatsParams params;

public:
  DetectStatsStage(const DetectStatsParams params);

  const DetectionResults operator()(const DetectionResults& input) override;

private:
  void extractDigit(const std::pair<size_t, cv::RotatedRect>& contourInfo,
                    const contours_t& allContours,
                    const std::vector<cv::Vec4i>& hierarchy,
                    std::vector<cv::Mat>& out) const;

  bool isDigit(const std::vector<cv::Point>& contour,
               const cv::RotatedRect& outline) const;

  contours_t getChildren(std::size_t parentIdx,
                         const contours_t& contours,
                         const std::vector<cv::Vec4i>& hierarchy) const;

  cv::Mat doDrawContours(const std::vector<cv::Point>& exterior,
                         const contours_t& holes) const;

  std::vector<std::pair<size_t, cv::RotatedRect>> findCandidates(
    const cv::RotatedRect& outline,
    const contours_t& allContours,
    const std::vector<cv::Vec4i>& hierarchy) const;

  void filterByHeight(
    std::vector<std::pair<size_t, cv::RotatedRect>>& contourInfo) const;

  bool hasOverlap(const std::pair<size_t, cv::RotatedRect>& info) const;

  std::pair<int, int> findOverlap(const std::vector<cv::Point>& contour) const;

  contours_t splitContour(const std::vector<cv::Point>& contour,
                          const std::pair<int, int> overlapIdxs) const;
};

#endif // OVERWATCHVISION_STATDETECTIONSTAGE_H
