#include "results.h"

const int NUM_MEDALS = 6;
/** Last stat is slightly lower vertically, so medal<->stat dist needs to be
 * adjusted. */
const double LAST_STAT_DIST_MULTIPLIER = 1.08;

bool
DetectionResults::isValid() const
{
  const auto notEmpty = [](const auto& s) { return !s.empty(); };
  const bool validStats = stats.size() == NUM_MEDALS &&
                          std::all_of(stats.begin(), stats.end(), notEmpty);

  return medals.size() == NUM_MEDALS && validStats && !outcome.empty();
}

float
DetectionResults::getAngle() const
{
  if (medals.empty()) {
    return 0;
  }

  std::vector<cv::Point> medalPts;
  for (const auto& m : medals) {
    medalPts.push_back(m.first);
  }

  cv::Vec4d line;
  cv::fitLine(cv::Mat(medalPts), line, cv::DIST_L2, 0, 0.01, 0.01);
  const auto vx = line[0];
  const auto vy = line[1];
  return (float)std::atan(vy / vx);
}

std::vector<cv::RotatedRect>
DetectionResults::calculateStatOutlines(int medalToStatDist,
                                        cv::Size statSize) const
{
  const auto angle = getAngle();
  const auto rotatePt = [&](const auto x, const auto y) {
    return cv::Point2f(x * std::cos(angle) - y * std::sin(angle),
                       x * std::sin(angle) + y * std::cos(angle));
  };

  std::vector<cv::RotatedRect> outlines;
  for (std::size_t i = 0; i < medals.size(); i++) {
    auto dist = scaleFactor * medalToStatDist;
    if (i == medals.size() - 1) {
      dist = (int)round(dist * LAST_STAT_DIST_MULTIPLIER);
    }
    const auto statCenter = medals[i].first + rotatePt(0, dist);
    const cv::Size rectSize((int)(statSize.width * scaleFactor),
                            (int)(statSize.height * scaleFactor));
    const cv::RotatedRect rect(
      statCenter, rectSize, (float)(angle * 180 / CV_PI));
    // Validation: outline must fit within image - abort if not.
    const auto br = rect.boundingRect().br();
    if (br.x > processed.binary.cols || br.y > processed.binary.rows) {
      break;
    }
    outlines.push_back(rect);
  }
  return outlines;
}

bool
RecognitionResults::isValid() const
{
  return stats.size() == NUM_MEDALS && detected.isValid();
}
