#ifndef OVERWATCHVISION_DATA_H
#define OVERWATCHVISION_DATA_H

#include <opencv2/opencv.hpp>
#include <string>

/** Entries are center point and radius. */
typedef std::vector<std::pair<cv::Point2f, float>> medals_t;
typedef std::vector<std::vector<cv::Point>> contours_t;
typedef std::vector<std::vector<cv::Mat>> stats_t;
typedef std::list<std::pair<char, cv::Mat>> training_data_t;

/**
 * Results of pre-processing stages, which do whole-image manipulation.
 */
struct PreProcessResults
{
  cv::Mat color;
  cv::Mat gray;
  cv::Mat binary;
};

/**
 * Results of detection stages, such as feature positions and region-of-interest
 * subimages.
 */
struct DetectionResults
{
  const PreProcessResults processed;
  double scaleFactor;
  medals_t medals;
  stats_t stats = stats_t();
  /** Victory/defeat/draw text at the top-left of screen. */
  cv::Rect2f outcome = cv::Rect2f();

  bool isValid() const;

  /** Rotation angle of results, based on position of detected medals. */
  float getAngle() const;

  /**
   * Determine approximate outline for each stat based on already found medals.
   * @param medalToStatDist
   * @param statSize
   * @return
   */
  std::vector<cv::RotatedRect> calculateStatOutlines(int medalToStatDist,
                                                     cv::Size statSize) const;
};

/**
 * Results of recognition stages, such as OCR.
 */
struct RecognitionResults
{
  const DetectionResults detected;
  std::vector<std::string> stats;

  bool isValid() const;
};

#endif // OVERWATCHVISION_DATA_H
