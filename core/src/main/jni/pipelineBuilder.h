#ifndef OVERWATCHVISION_PIPELINEBUILDER_H
#define OVERWATCHVISION_PIPELINEBUILDER_H

#include "results.h"
#include "stages/convertInputStage.h"
#include <functional>

std::function<RecognitionResults(cv::Mat)>
buildPipeline(double scaleFactor,
              const InputParams& inputParams,
              const std::string& statModelFile);

#endif // OVERWATCHVISION_PIPELINEBUILDER_H
