package com.chasecaleb.overwatchvision.core

import android.content.Context
import org.opencv.android.OpenCVLoader
import java.io.Closeable
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Computer vision-based detection for camera frames.
 *
 * Constructor params are assumed to be constant for all frames.
 *
 * @param rotated True if frame needs to be rotated 180 degrees. Sensors of some devices (e.g. Nexus 5X/6P) are
 * mounted upside down compared to the screen - frame data is not corrected for this.
 */
class FrameDetector(
        rotated: Boolean,
        frameWidth: Int,
        frameHeight: Int,
        targetWidth: Int,
        context: Context
) : Closeable {

    companion object {
        init {
            val initSuccess = OpenCVLoader.initDebug()
            if (!initSuccess) {
                throw IllegalStateException("Failed to load OpenCV native library")
            }
            System.loadLibrary("core")
        }
    }

    // Used from native code. Stores pointer to native object.
    @Suppress("unused")
    private var nativeHandle: Long = 0
    private var didClose = AtomicBoolean(false)
    private val statModelData = context.filesDir.resolve("stat_classifier.yaml")

    init {
        // Need to copy training data from resource (packed inside of apk) to filesystem so that
        // native OpenCV's FileStorage can open it.
        // Copying file every time to avoid caching an old version (oops...).
        context.resources.openRawResource(R.raw.stat_classifier).use { res ->
            statModelData.outputStream().use { res.copyTo(it) }
        }

        initialize(frameWidth, frameHeight, rotated, targetWidth, statModelData.absolutePath)
    }

    @Synchronized
    fun detect(nv21Bytes: ByteArray): DetectionResults {
        // Invoking native code after close/free would be... well, not good.
        // Safeguard is necessary since detection is invoked from a worker thread.
        if (didClose.get()) {
            return DetectionResults()
        }

        val results = DetectionResults()
        doDetect(nv21Bytes, results)
        return results
    }

    @Synchronized
    override fun close() {
        // Prevent potential double-free in native code.
        if (didClose.getAndSet(true)) {
            return
        }
        doClose()
    }

    private external fun doClose()

    private external fun initialize(frameWidth: Int, frameHeight: Int, rotated: Boolean, targetWidth: Int,
                                    statModelFile: String)

    private external fun doDetect(nv21Bytes: ByteArray, results: DetectionResults)

}