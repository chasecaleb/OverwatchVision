package com.chasecaleb.overwatchvision.core

import java.io.Serializable

/**
 * Contains detection results from native computer vision pipeline.
 *
 * *Implementation note:* This is a mutable non-data class in order to simplify native code.
 */
class DetectionResults : Serializable {
    companion object {
        val STAT_TITLES = listOf(
                "Eliminations",
                "Objective Kills",
                "Objective Time",
                "Damage Done",
                "Healing Done",
                "Deaths"
        )
    }

    var stats = emptyList<Int>()
        private set
    val isValid get() = stats.isNotEmpty()

    /**
     * Called from native code. Takes an array instead of list to simplify native interop.
     */
    @Suppress("unused")
    fun setStats(values: IntArray) {
        stats = values.toList()
        if (stats.size != STAT_TITLES.size) {
            throw IllegalStateException("Unexpected number of stats: ${stats.size}")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return stats == (other as DetectionResults).stats
    }

    override fun hashCode(): Int = stats.hashCode()


}