# OverwatchVision
> Automatic computer-vision based stat tracking for Overwatch.

## Demo
[![Youtube](http://img.youtube.com/vi/jwopQJa0Uyc/0.jpg)](http://www.youtube.com/watch?v=jwopQJa0Uyc)

Click the image (above) to view demo on YouTube.

## Installation
Requirement: Android 5.0 or higher.

Download latest APK from [releases](https://github.com/chasecaleb/OverwatchVision/releases) for your
phone architecture. If in doubt, try arm64-v8a and armeabi-v7a first. Whichever installs without
failing is the correct one.

## Development Setup
* Install latest release of Android Studio (3.0 as of 12/2017, but may work with 2.2+)
* Clone this repo and open in Android Studio
* If not automatically prompted, open the Android SDK Manager and install the NDK and CMake.
If you get stuck, refer to [the Android NDK tutorial](https://developer.android.com/ndk/guides/index.html).
* Build with Gradle
* Sample images for prototyping are available in `core/src/androidTest/assets`

*Note*: While the app does work on an emulator, I strongly recommend against it. This is a very
camera-dependent application and emulators have poor camera support. Also keep in mind that built-in
laptop webcams may be too low quality for this.