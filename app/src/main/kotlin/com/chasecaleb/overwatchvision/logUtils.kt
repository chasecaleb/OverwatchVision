package com.chasecaleb.overwatchvision

import android.util.Log

fun Any.logD(msg: () -> String) {
    ifDebugBuild { Log.d(this::class.java.tag, msg()) }
}

fun Any.logD(ex: Throwable, msg: () -> String) {
    ifDebugBuild { Log.d(this::class.java.tag, msg(), ex) }
}

fun Any.logI(msg: () -> String) {
    ifDebugBuild { Log.i(this::class.java.tag, msg()) }
}

fun Any.logI(ex: Throwable, msg: () -> String) {
    ifDebugBuild { Log.i(this::class.java.tag, msg(), ex) }
}

fun Any.logW(msg: () -> String) {
    ifDebugBuild { Log.w(this::class.java.tag, msg()) }
}

fun Any.logW(ex: Throwable, msg: () -> String) {
    ifDebugBuild { Log.w(this::class.java.tag, msg(), ex) }
}

fun Any.logE(msg: () -> String) {
    ifDebugBuild { Log.d(this::class.java.tag, msg()) }
}

fun Any.logE(ex: Throwable, msg: () -> String) {
    ifDebugBuild { Log.d(this::class.java.tag, msg(), ex) }
}

fun ifDebugBuild(action: () -> Unit) {
    if (BuildConfig.DEBUG) {
        action()
    }
}


private val Class<*>.tag
    get(): String =
        this.simpleName.takeIf { it.isNotBlank() } ?: this.enclosingClass.tag