package com.chasecaleb.overwatchvision

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import com.chasecaleb.overwatchvision.capture.CaptureActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * Main activity shown on app startup.
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivityUi().setContentView(this)
    }

    fun doRecordStats() {
        // Need camera permission if not already granted.
        // Check every time because the request may have been denied or previously granted and then revoked.
        val permission = Manifest.permission.CAMERA
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(permission), 0)
            return
        }
        startActivity<CaptureActivity>()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // Camera permission to record stats is the only possible trigger - don't need to check request code.
        doRecordStats()
    }
}

class MainActivityUi : AnkoComponent<MainActivity> {
    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        verticalLayout {
            padding = dip(30)
            button(R.string.recordStats) {
                onClick { owner.doRecordStats() }
            }
        }
    }
}