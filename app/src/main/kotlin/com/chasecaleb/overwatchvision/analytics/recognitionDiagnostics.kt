import android.content.Context
import android.graphics.ImageFormat
import android.graphics.Rect
import android.graphics.YuvImage
import android.os.Build
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectMetadata
import com.chasecaleb.overwatchvision.BuildConfig
import com.chasecaleb.overwatchvision.core.DetectionResults
import io.fotoapparat.preview.Frame
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import java.io.File
import java.lang.Exception
import java.util.*

typealias UploadDetection = (DetectionResults, Frame) -> Unit

fun createDetectionUploader(context: Context): UploadDetection {
    initializeAws(context)
    val uploader = TransferUtility.builder()
            .context(context)
            .awsConfiguration(AWSMobileClient.getInstance().configuration)
            .s3Client(AmazonS3Client(AWSMobileClient.getInstance().credentialsProvider))
            .build()

    return { results, frame ->
        val file = File(context.cacheDir, UUID.randomUUID().toString() + ".jpg")
        frame.writeJpg(file)

        val metadata = ObjectMetadata().also {
            it.userMetadata = results.getUploadMetadata()
        }
        uploader.upload(
                "uploads/" + file.name,
                file,
                metadata,
                null,
                CleanupTransferListener(file)
        )
    }
}

private fun initializeAws(context: Context) {
    AWSMobileClient.getInstance()
            .initialize(context) { _ -> /* Don't care about listener. */ }
            .execute()
}

private fun Frame.writeJpg(output: File) {
    // TODO should rotate 180 degrees on some special devices (*cough* nexus 5x)
    val yuv = YuvImage(image, ImageFormat.NV21, size.width, size.height, null)
    output.outputStream().use {
        yuv.compressToJpeg(Rect(0, 0, size.width, size.height), 100, it)
    }
}

private fun DetectionResults.getUploadMetadata() = mapOf(
        "resultStats" to stats.toString(),
        "appVersion" to BuildConfig.VERSION_NAME,
        "appIsDebug" to BuildConfig.DEBUG.toString(),
        "androidRelease" to Build.VERSION.RELEASE,
        "deviceManufacturer" to Build.MANUFACTURER,
        "deviceModel" to Build.MODEL
)

private class CleanupTransferListener(private val file: File) : TransferListener {
    companion object {
        private val LOG = AnkoLogger<CleanupTransferListener>()
        private val END_STATES = listOf(
                TransferState.COMPLETED,
                TransferState.FAILED,
                TransferState.CANCELED
        )
    }

    override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
        // Don't care.
    }

    override fun onStateChanged(id: Int, state: TransferState?) {
        LOG.debug { "Upload state for ${file.name}: $state" }
        if (state in END_STATES) {
            LOG.info { "Upload outcome for ${file.name}: $state" }
            file.delete()
        }
    }

    override fun onError(id: Int, ex: Exception?) {
        // Upload is just for analytics/algorithm optimisation, so user doesn't care.
        LOG.error { "Upload failed for ${file.name}: $ex" }
    }
}