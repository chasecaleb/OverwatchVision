package com.chasecaleb.overwatchvision.capture

import android.content.Context
import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import com.chasecaleb.overwatchvision.core.DetectionResults
import com.chasecaleb.overwatchvision.core.FrameDetector
import com.chasecaleb.overwatchvision.logD
import com.chasecaleb.overwatchvision.logI
import io.fotoapparat.Fotoapparat
import io.fotoapparat.parameter.Resolution
import io.fotoapparat.preview.Frame
import io.fotoapparat.preview.FrameProcessor
import java.io.Closeable
import java.util.concurrent.Semaphore

/**
 * Handles high-level camera preview [Frame] processing.
 *
 * @see FrameDetector
 */
class FrameHandler(
        private val context: Context,
        private val targetSizeCallback: (Rect) -> Unit,
        private val resultsCallback: (DetectionResults, Frame) -> Unit
) : FrameProcessor, Closeable {

    companion object {
        private val UI_HANDLER = Handler(Looper.getMainLooper())
    }

    var fotoapparat: Fotoapparat? = null
    private var previousResult: DetectionResults? = null
    private var frameCount = 0
    private var startTime = System.nanoTime()
    private var previousSize: Resolution? = null
    private var previousRotation: Int = 0
    private val lock = Semaphore(1)
    /** Cached value computed by [getTargetOutline]. */
    private var cachedTargetOutline: Rect? = null
    /** Cached value computed by [getDetector]. */
    private var cachedDetector: FrameDetector? = null

    override fun process(frame: Frame) {
        if (!lock.tryAcquire()) {
            return
        }
        maybeReset(frame)
        logFramerate()

        val detector = getDetector(frame.size, frame.rotation, getTargetOutline(frame.size))
        val result = detector.detect(frame.image)
        if (!result.isValid) {
            lock.release()
        } else if (result != previousResult) {
            logD { "New result: ${result.stats}, previous: ${previousResult?.stats}" }
            // Only updating previous when valid to avoid taking forever if intermittent.
            // The point of double-checking is to make sure the final detection is done against an
            // in-focus image without having to trigger autofocus for every single frame, which
            // would give users a headache (trust me...).
            previousResult = result

            // Needs to be in a different thread or else Fotoapparat will block itself.
            fotoapparat?.safeAsyncRefocus(lock::release) ?: lock.release()
        } else {
            // Fotoapparat uses image pool, so passing outside this thread is not safe.
            val frameCopy = frame.copy(image = frame.image.copyOf())
            UI_HANDLER.post { resultsCallback(result, frameCopy) }
            lock.release()
        }
    }

    @Synchronized
    override fun close() {
        cachedTargetOutline = null
        cachedDetector?.close()
        cachedDetector = null
    }

    /** Clears cached objects to trigger reinitalization if frame metadata (e.g. size) changes. */
    private fun maybeReset(frame: Frame) {
        if (frame.size != previousSize || frame.rotation != previousRotation) {
            logI { "Frame changed to ${frame.size} @ ${frame.rotation}" }
            close()
            previousSize = frame.size
            previousRotation = frame.rotation
        }
    }

    private fun logFramerate() {
        frameCount++
        if (frameCount % 5 == 0) {
            val elapsedSecs: Double = (System.nanoTime() - startTime) / 1_000_000_000.0
            val fps = frameCount / elapsedSecs
            logD { "Framerate: $fps fps" }
        }
    }

    /** @return [cachedTargetOutline] (will be instantiated if needed). */
    private fun getTargetOutline(frameSize: Resolution): Rect =
            cachedTargetOutline ?: frameSize
                    .calculateTargetOutline(0.85f)
                    .also {
                        cachedTargetOutline = it
                        targetSizeCallback(it)
                    }

    /** @return [cachedDetector] (will be instantiated if needed). */
    private fun getDetector(frameSize: Resolution, frameRotation: Int, target: Rect): FrameDetector =
            cachedDetector ?: FrameDetectorConfig(frameSize, frameRotation, target)
                    .createDetector(context)
                    .also {
                        cachedDetector = it
                    }
}