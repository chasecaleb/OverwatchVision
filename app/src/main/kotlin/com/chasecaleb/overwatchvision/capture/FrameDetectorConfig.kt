package com.chasecaleb.overwatchvision.capture

import android.content.Context
import android.graphics.Rect
import com.chasecaleb.overwatchvision.core.FrameDetector
import io.fotoapparat.parameter.Resolution

data class FrameDetectorConfig(
        val frameSize: Resolution,
        val frameRotation: Int,
        val target: Rect
)

fun FrameDetectorConfig.createDetector(context: Context): FrameDetector {
    val targetW = target.let {
        if (it.bottom < it.right) {
            it.width()
        } else {
            it.height()
        }
    }

    return FrameDetector(
            rotated = frameRotation == 180,
            frameWidth = frameSize.width,
            frameHeight = frameSize.height,
            targetWidth = targetW,
            context = context
    )
}