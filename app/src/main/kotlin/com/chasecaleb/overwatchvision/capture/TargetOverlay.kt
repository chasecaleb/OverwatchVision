package com.chasecaleb.overwatchvision.capture

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.View
import io.fotoapparat.parameter.Resolution
import kotlin.properties.Delegates

/**
 * Overlay displayed during capture to help user align the screen.
 */
class TargetOverlay(context: Context) : View(context) {
    companion object {
        /** Width of screen that measurement constants are relative to.  */
        private const val MEASUREMENT_WIDTH = 1920.0f
        private const val MEDAL_COUNT = 6
        private const val MEDAL_RADIUS = 65.0f
        /** X-axis distance in between adjacent medals. */
        private const val MEDAL_SPACING = 270.0f
        private const val MEDAL_Y = 350.0f
        /** Y-axis distance from medal centerpoint to the top of the stat text below it. */
        private const val STAT_OFFSET = 85.0f
        /** Width, height (respectively) of stat outlines. */
        private val STAT_DIMENS = 175.0f to 75.0f
        private val PAINT = Paint().apply {
            style = Paint.Style.STROKE
            strokeWidth = 5.0f
            color = Color.RED
        }
    }

    // Note: Properties are observable to automatically redraw view when changed.
    /** Dimensions of camera preview. Needed to scale overlay properly. */
    var previewSize by Delegates.observable(null as Resolution?) { _, _, _ -> postInvalidate() }
    var outline by Delegates.observable(null as Rect?) { _, _, _ -> postInvalidate() }


    // The amount of allocation is insignificant and it isn't redrawn frequently.
    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // Preview and canvas are the same aspect ratio, so scale both sides equally.
        val previewScale = previewSize?.let { canvas.width / it.width.toFloat() } ?: return
        val currentOutline = outline ?: return
        val scaledOutline = RectF(currentOutline.left * previewScale,
                currentOutline.top * previewScale,
                currentOutline.right * previewScale,
                currentOutline.bottom * previewScale)
        canvas.drawRect(scaledOutline, PAINT)

        val detailScale = scaledOutline.width() / MEASUREMENT_WIDTH
        val medalWidth = (MEDAL_COUNT - 1) * MEDAL_SPACING * detailScale
        val medalOffsetX = scaledOutline.left + (scaledOutline.width() - medalWidth) / 2.0f
        (0 until MEDAL_COUNT).forEach { i ->
            val medalX = medalOffsetX + i * MEDAL_SPACING * detailScale
            val medalY = scaledOutline.top + MEDAL_Y * detailScale
            canvas.drawCircle(medalX, medalY, MEDAL_RADIUS * detailScale, PAINT)

            val statTop = medalY + STAT_OFFSET * detailScale
            val statWidth = STAT_DIMENS.first * detailScale
            canvas.drawRect(
                    medalX - statWidth / 2,
                    statTop,
                    medalX + statWidth / 2,
                    statTop + STAT_DIMENS.second * detailScale,
                    PAINT)
        }
    }
}