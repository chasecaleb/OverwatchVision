package com.chasecaleb.overwatchvision.capture

import android.graphics.Rect
import com.chasecaleb.overwatchvision.logW
import io.fotoapparat.Fotoapparat
import io.fotoapparat.configuration.UpdateConfiguration
import io.fotoapparat.parameter.FocusMode
import io.fotoapparat.parameter.Resolution
import io.fotoapparat.selector.FocusModeSelector
import io.fotoapparat.selector.single
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.info

private val LOG = AnkoLogger("captureUtils")

/** Scale [sourceW] and [sourceH] equally to fit within [destRatio]. */
fun fitDimensions(sourceW: Int, sourceH: Int, destRatio: Float): Pair<Int, Int> =
        if (destRatio > sourceW / sourceH.toFloat()) {
            sourceW to (sourceW / destRatio).toInt()
        } else {
            (sourceH * destRatio).toInt() to sourceH
        }

/** Calculate target outline to show as overlay. */
fun Resolution.calculateTargetOutline(relativeSize: Float): Rect {
    val (targetW, targetH) = fitDimensions(
            sourceW = width,
            sourceH = height,
            destRatio = 16 / 9.0f
    ).let {
        (it.first * relativeSize).toInt() to (it.second * relativeSize).toInt()
    }
    val paddingW = (width - targetW) / 2
    val paddingH = (height - targetH) / 2

    return Rect(paddingW, paddingH, paddingW + targetW, paddingH + targetH)
}

/**
 * Forces camera to do a full autofocus cycle in the background.
 *
 * Swallows exceptions, if any, and calls [onFinish] when done.
 *
 * This is useful even in continuous focus mode, because continuous aims for "good enough"
 * to avoid constantly scanning... not to mention that there's no telling if it's in mid-focus
 * at any given time or not.
 */
fun Fotoapparat.safeAsyncRefocus(onFinish: () -> Unit) {
    bg {
        try {
            refocus()
        } catch (ex: IllegalStateException) {
            // Can happen if attempting to focus while camera is shutting down. Don't care.
            logW { "Caught exception while refocusing: $ex" }
        } finally {
            onFinish()
        }
    }
}

private fun Fotoapparat.refocus() {
    if (!getCapabilities().await().focusModes.contains(FocusMode.Auto)) {
        LOG.info { "Aborting - device does not support autofocus" }
        return
    }

    val original = getCurrentParameters().await().focusMode

    val configureFocus = { mode: FocusModeSelector ->
        updateConfiguration(UpdateConfiguration(
                focusMode = mode
        )).get()
    }


    configureFocus(io.fotoapparat.selector.autoFocus())
    focus().await()
    configureFocus(single(original))
}
