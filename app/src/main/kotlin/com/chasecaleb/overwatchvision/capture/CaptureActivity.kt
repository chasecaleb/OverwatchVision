package com.chasecaleb.overwatchvision.capture

import UploadDetection
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.FrameLayout
import com.chasecaleb.overwatchvision.core.DetectionResults
import com.chasecaleb.overwatchvision.logI
import createDetectionUploader
import io.fotoapparat.Fotoapparat
import io.fotoapparat.parameter.Resolution
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.preview.Frame
import io.fotoapparat.selector.*
import io.fotoapparat.view.CameraView
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.customView
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Captures stats using camera and computer vision/ocr.
 *
 * Precondition: Camera permission must already be granted.
 */
class CaptureActivity : AppCompatActivity() {
    private lateinit var ui: CaptureActivityUi
    private lateinit var fotoapparat: Fotoapparat
    private lateinit var doUpload: UploadDetection
    private var waitingForResult = AtomicBoolean(false)
    private var frameHandler: FrameHandler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui = CaptureActivityUi().also { it.setContentView(this) }
        val handler = FrameHandler(applicationContext,
                { ui.targetOverlay.outline = it },
                this::handleResults
        ).also { frameHandler = it }

        val sizeSelector = wideRatio({
            this.sortedBy { it.width }.firstOrNull { it.width >= 900 }
        })

        fotoapparat = Fotoapparat.with(applicationContext).into(ui.preview)
                .lensPosition(back())
                .previewScaleType(ScaleType.CenterInside)
                .photoResolution(sizeSelector)
                .previewResolution(sizeSelector)
                .focusMode(firstAvailable(
                        continuousFocusPicture(),
                        continuousFocusVideo(),
                        autoFocus(),
                        fixed()))
                .frameProcessor(handler)
                .build()
        handler.fotoapparat = fotoapparat

        doUpload = createDetectionUploader(this)
    }

    private fun handleResults(detectionResults: DetectionResults, frame: Frame) {
        val isWaiting = waitingForResult.getAndSet(false)
        if (!isWaiting) {
            return
        }

        doUpload(detectionResults, frame)
        startActivity<CaptureSuccessActivity>(CaptureSuccessActivity.RESULTS_KEY to detectionResults)
        finish()
    }

    override fun onResume() {
        super.onResume()
        waitingForResult.set(true)
        fotoapparat.start()
        fotoapparat.getCurrentParameters().whenAvailable { params ->
            params?.previewResolution?.also { size ->
                ui.fitPreviewSize(size)
            }
        }
    }

    @Synchronized
    override fun onPause() {
        super.onPause()
        waitingForResult.set(false)
        try {
            fotoapparat.stop()
        } catch (ex: IllegalStateException) {
            // Thrown if already stopped - don't care, since it's obviously finished either way.
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        frameHandler?.close()
        frameHandler = null
    }
}

class CaptureActivityUi : AnkoComponent<CaptureActivity> {
    private lateinit var frame: FrameLayout
    lateinit var preview: CameraView
    lateinit var targetOverlay: TargetOverlay

    override fun createView(ui: AnkoContext<CaptureActivity>) = with(ui) {
        // Camera preview may have margins due to aspect ratio, so background color and alignment matter.
        owner.window.decorView.backgroundColor = Color.BLACK
        frameLayout {
            frame = this
            lparams {
                gravity = Gravity.CENTER
            }
            customView<CameraView> {
                preview = this
            }
            customView<TargetOverlay> {
                targetOverlay = this
            }
        }
    }

    /**
     * Resize camera/overlay views to fit the same aspect ratio as [cameraPreview].
     */
    fun fitPreviewSize(cameraPreview: Resolution) {
        val (displayW, displayH) = fitDimensions(
                sourceW = frame.width,
                sourceH = frame.height,
                destRatio = cameraPreview.width / cameraPreview.height.toFloat())
        frame.layoutParams = frame.layoutParams.also {
            it.width = displayW
            it.height = displayH
        }
        targetOverlay.previewSize = cameraPreview
        logI { "Camera preview size: $cameraPreview" }
    }
}