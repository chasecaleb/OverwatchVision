package com.chasecaleb.overwatchvision.capture

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.widget.GridLayout
import com.chasecaleb.overwatchvision.core.DetectionResults
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * Displays successful results from [CaptureActivity].
 */
class CaptureSuccessActivity : AppCompatActivity() {
    companion object {
        const val RESULTS_KEY = "results"
    }

    private lateinit var ui: CaptureSuccessActivityUi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val results = intent?.extras?.get(RESULTS_KEY) as? DetectionResults
                ?: throw IllegalStateException("Intent missing results")
        ui = CaptureSuccessActivityUi(results).also { it.setContentView(this) }
    }
}

class CaptureSuccessActivityUi(private val results: DetectionResults) : AnkoComponent<CaptureSuccessActivity> {

    override fun createView(ui: AnkoContext<CaptureSuccessActivity>): View = with(ui) {
        gridLayout {
            columnCount = 2
            results.stats
                    .mapIndexed { i, value -> DetectionResults.STAT_TITLES[i] to value }
                    .forEach { (title, value) ->
                        val addText = { it: String ->
                            textView(it) {
                                textSize = 18.0F
                            }.lparams {
                                setGravity(Gravity.END)
                                padding = dip(20)
                            }
                        }
                        addText("$title:")
                        addText(value.toString())
                    }
            button("Finish") {
                onClick { owner.finish() }
            }.lparams {
                topMargin = dip(20)
                columnSpec = GridLayout.spec(0, 2)
                setGravity(Gravity.CENTER_HORIZONTAL)
            }
        }
    }
}